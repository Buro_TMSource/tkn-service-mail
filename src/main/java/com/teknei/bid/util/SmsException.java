package com.teknei.bid.util;

public class SmsException extends Exception {

    public SmsException(ErrorSmsCode errorSmsCode) {
        super("Sms error type reported: " + errorSmsCode.name() + " at Epoch: " + System.currentTimeMillis() / 1000);
    }

}