package com.teknei.bid.services;

import com.teknei.bid.controller.rest.crypto.Decrypt;
import com.teknei.bid.dto.MailSendDataDTO;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import com.teknei.bid.util.ErrorSmsCode;
import com.teknei.bid.util.SmsException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SendMailTask {


    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private BidOtpRepository otpRepository;
    @Autowired
    private BidClieRepository clieRepository;
    @Autowired
    private BidClieMailRepository mailRepository;
    @Autowired
    private BidTelRepository bidTelRepository;
    @Autowired
    private BidCelRepository bidCelRepository;
    @Autowired
    private TasManager tasManager;
    @Value("${tkn.sms.urlRoot}")
    private String urlRoot;
    @Value("${tkn.sms.userParamName}")
    private String userParamName;
    @Value("${tkn.sms.pwdParamName}")
    private String passwordParamName;
    @Value("${tkn.sms.numberParamName}")
    private String numberParamName;
    @Value("${tkn.sms.localPrefix}")
    private String localPrefix;
    @Value("${tkn.sms.messageParamName}")
    private String messageParamName;
    @Value("${tkn.sms.secretName}")
    private String secretName;
    @Value("${tkn.sms.template}")
    private String template;
    private String username;
    private String password;
    @Autowired
    private Decrypt decrypt;
    @Value("${tkn.config.otp_provider}")
    private String otpProvider;
    @Value("${tkn.vc.uri}")
    private String bidWebAppUri;
    private static final short TYPE_MOBILE = 0;
    private static final short TYPE_HOME = 1;

    private static final Logger log = LoggerFactory.getLogger(SendMailTask.class);

    @PostConstruct
    private void postConstruct() {
        readUsernamePassword();
    }

    private void readUsernamePassword() {
        final String secretUri = "/run/secrets/" + secretName;
        String user = null;
        String pass = null;
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            if (content == null || content.isEmpty()) {
                log.error("No secret supplied, leaving default");
            } else {
                JSONObject jsonObject = new JSONObject(content);
                user = jsonObject.optString("username", username);
                pass = jsonObject.optString("password", password);
                user = decrypt(user);
                pass = decrypt(pass);
                username = user;
                password = pass;
            }
        } catch (IOException e) {
            log.error("No secret supplied, leaving default");
        }
    }

    @Scheduled(fixedRate = 30000)
    public void sendPendings() {
        List<BidOtp> listPendindgs = null;
        try {
            listPendindgs = otpRepository.findBySend(false);
        } catch (Exception e) {
            log.error("Error finding OTP pendings with message: {}", e.getMessage());
            return;
        }
        if (listPendindgs == null || CollectionUtils.isEmpty(listPendindgs)) {
            return;
        }
        if (otpProvider.trim().toUpperCase().equals("SMS")) {
            listPendindgs.forEach(c -> {
                try {
                    if(isNumeric(c)){
                        sendSMS(c);
                    }else{
                        sendMailToVideocall(c);
                    }
                } catch (SmsException e) {
                    log.error("SMS Error reached: {} at: {}", e.getMessage(), System.currentTimeMillis());
                    if (e.getMessage().contains(ErrorSmsCode.INTERNAL_API_NUMBER_NOT_VALID.name()) || e.getMessage().contains(ErrorSmsCode.INTERNAL_API_NOT_NUMBER_PROVIDED.name())) {
                        log.info("Send backup OTP via email");
                        sendMail(c);
                    }
                }
            });
        } else {
            listPendindgs.forEach(c -> {
                if(isNumeric(c)){
                    sendMail(c);
                }else{
                    sendMailToVideocall(c);
                }
            });
        }

    }

    public boolean isNumeric(BidOtp bidOtp){
        try{
            Integer.parseInt(bidOtp.getOtp());
            return true;
        }catch (NumberFormatException ne){
            return false;
        }
    }

    public void sendMailContract(Long idClie) throws MessagingException {
        MailSendDataDTO dataDTO = formatData(idClie, "NA");
        byte[] contract = tasManager.getContract(String.valueOf(idClie));
        sendMailWithContract(dataDTO, contract);
    }

    private void sendSMS(BidOtp bidOtp) throws SmsException {
        List<BidCel> bidCelList = bidCelRepository.findAllByIdClieAndIdEsta(bidOtp.getIdClie(), 1);
        if (bidCelList == null || bidCelList.isEmpty()) {
            throw new SmsException(ErrorSmsCode.INTERNAL_API_NOT_NUMBER_PROVIDED);
        }
        List<String> validTels = bidCelList.stream().filter(t -> t.getNumCel().length() == 10).map(t -> t.getNumCel()).collect(Collectors.toList());
        if (validTels == null || validTels.isEmpty()) {
            throw new SmsException(ErrorSmsCode.INTERNAL_API_NUMBER_NOT_VALID);
        }
        String numberToSend = localPrefix + validTels.get(0).trim();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        String content = template;
        content = content.replace("{0}", bidOtp.getOtp());
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlRoot)
                .queryParam(userParamName, username)
                .queryParam(passwordParamName, password)
                .queryParam(numberParamName, numberToSend)
                .queryParam(messageParamName, content);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<?> entity = new HttpEntity<>(headers);
        try {
            HttpEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, String.class);
            JSONObject jsonObject = new JSONObject(response.getBody());
            Boolean success = jsonObject.getBoolean("success");
            Integer code = jsonObject.getInt("code");
            Long id = jsonObject.getLong("id");
            if (!success) {
                log.error("No success in send SMS to: {} with code: {} and description: {}", numberToSend, code, ErrorSmsCode.find(code));
                throw new SmsException(ErrorSmsCode.find(code));
            }
            log.info("SMS sent successfully: {}", jsonObject);
        } catch (RestClientException e) {
            log.error("Error calling SMS API with message: {}", e.getMessage());
            throw new SmsException(ErrorSmsCode.INTERNAL_API_UNKNOWN_ERROR);
        } catch (Exception e) {
            log.error("Unknown error: {}", e.getMessage());
            throw new SmsException(ErrorSmsCode.INTERNAL_API_UNKNOWN_ERROR);
        } finally {
            MailSendDataDTO dto = new MailSendDataDTO();
            dto.setIdClient(bidOtp.getIdClie());
            dto.setOtp(bidOtp.getOtp());
            deleteOTP(dto);
        }

    }

    private void sendMailToVideocall(BidOtp otp){
        MailSendDataDTO mailSendDataDTO = formatData(otp.getIdClie(), otp.getOtp());
        if (mailSendDataDTO == null) {
            return;
        }
        sendMailToVideocallInternal(mailSendDataDTO);
    }

    private void sendMail(BidOtp otp) {
        MailSendDataDTO mailSendDataDTO = formatData(otp.getIdClie(), otp.getOtp());
        if (mailSendDataDTO == null) {
            return;
        }
        sendMailInternal(mailSendDataDTO);
    }

    private MailSendDataDTO formatData(Long idClie, String otp) {
        MailSendDataDTO mailSendDataDTO = new MailSendDataDTO();
        mailSendDataDTO.setIdClient(idClie);
        BidClieMailPK mailPK = new BidClieMailPK();
        mailPK.setIdClie(idClie);
        mailPK.setIdEmai(idClie);
        try {
            BidClieMail bidClieMail = mailRepository.findOne(mailPK);
            String mail = bidClieMail.getEmai();
            mailSendDataDTO.setFullname("");
            mailSendDataDTO.setOtp(otp);
            mailSendDataDTO.setMail(mail);
            return mailSendDataDTO;
        } catch (Exception e) {
            log.error("Error finding informatio for custmer: {} searching preloaded mail", idClie);
            try {
                BidClieMail preloadedMail = mailRepository.findTopByIdClie(idClie);
                String mail = preloadedMail.getEmai();
                mailSendDataDTO.setFullname("");
                mailSendDataDTO.setOtp(otp);
                mailSendDataDTO.setMail(mail);
                return mailSendDataDTO;
            } catch (Exception e2) {
                log.error("Error finding informatio for custmer: {} and no preloaded one", idClie);
                return null;
            }
        }
    }

    private void sendMailWithContract(MailSendDataDTO dto, byte[] contentContract) throws MessagingException {
        String content = new StringBuilder("Estimado(a):")
                .append("\n")
                .append("Adjunto se encuentra su contrato generad por el sistema. El presente no tiene ninguna validez ni presenta ninguna obligacion con el banco, es solo de caracter informativo").toString();
        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setTo(dto.getMail());
        helper.setSubject("Contrato BID");
        helper.setText(content);
        ByteArrayDataSource source = new ByteArrayDataSource(contentContract, "application/pdf");
        helper.addAttachment("contrato.pdf", source);
        emailSender.send(mimeMessage);
    }

    private void sendMailToVideocallInternal(MailSendDataDTO dto){
        Runnable task = () -> {
            try {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(dto.getMail());
                message.setSubject("Videoconferencia");
                String otpContent = new StringBuilder("Estimado(a):")
                        .append("\n")
                        .append("Favor de introducir la siguiente direccion en su navegador para con el proceso de videoconferencia \n")
                        .append(bidWebAppUri)
                        .append("#!/webcall?idClient="+dto.getIdClient()+"&otp="+dto.getOtp()+"\n")
                        .toString();
                message.setText(otpContent);
                emailSender.send(message);
                deleteOTP(dto);
            } catch (Exception e) {
                log.error("Error sending mail to: {} with error message: {}", dto.getMail(), e.getMessage());
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }

    private void sendMailInternal(MailSendDataDTO dto) {
        Runnable task = () -> {
            try {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(dto.getMail());
                message.setSubject("Confirmar cuenta");
                String otpContent = new StringBuilder("Estimado(a):")
                        .append("\n")
                        .append("Favor de introducir el siguiente PIN en la pantalla para verificar su cuenta y continuar con el proceso \n")
                        .append(dto.getOtp()).toString();
                message.setText(otpContent);
                emailSender.send(message);
                deleteOTP(dto);
            } catch (Exception e) {
                log.error("Error sending mail to: {} with error message: {}", dto.getMail(), e.getMessage());
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }

    private void deleteOTP(MailSendDataDTO dto) {
        try {
            List<BidOtp> otpList = otpRepository.findByIdClieAndOtpAndUsed(dto.getIdClient(), dto.getOtp(), false);
            if (otpList == null || otpList.isEmpty()) {
                log.info("No otp found, bug?");
                return;
            }
            otpList.forEach(c -> {
                c.setSend(true);
                c.setFchSend(new Timestamp(System.currentTimeMillis()));
                otpRepository.save(c);
            });
        } catch (Exception e) {
            log.error("Error update to used otp: {} with message: {}", dto, e.getMessage());
        }
    }

    private String decrypt(String source) {
        try {
            String decrypted = decrypt.decrypt(source);
            return decrypted == null ? source : decrypted;
        } catch (Exception e) {
            log.warn("No ciphered content, returning clear");
            return source;
        }
    }

}