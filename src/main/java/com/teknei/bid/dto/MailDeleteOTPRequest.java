package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailDeleteOTPRequest implements Serializable {

    private String otp;
    private String key;

}