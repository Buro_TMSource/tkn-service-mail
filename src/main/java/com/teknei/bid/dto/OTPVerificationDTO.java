package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OTPVerificationDTO implements Serializable{

    private Long idClient;
    private String otp;
    private String username;

}